#!/bin/bash

# Validate commit messages
git log --format=%s $BITBUCKET_COMMIT ^$BITBUCKET_PREVIOUS_COMMIT | \
  grep -vE "^(feat|fix|docs|style|refactor|test|chore)(\([A-Z]+\))?: .{1,50}$" && \
  echo "Error: Commit messages must follow the conventional format." && \
  exit 1